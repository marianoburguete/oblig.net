﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationLayerWinform
{
    public partial class EditarEmpleado : Form
    {
        public EditarEmpleado()
        {
            InitializeComponent();
        }

        private void EditarEmpleado_Load(object sender, EventArgs e)
        {
            Controller c = new Controller();
            int id = ListadoPrincipal.empSeleccionado;
            Employee emp = c.GetEmployee(id);
            if (emp.GetType().Name == "FullTimeEmployee")
            {
                label5.Text = "Salario:";
                FullTimeEmployee fte = (FullTimeEmployee)emp;
                textBoxNombre.Text = fte.Name;
                dateTimePicker.Value = fte.StartDate;
                textBoxTipo.Text = fte.Salary.ToString();
            }
            else
            {
                label5.Text = "Por hora:";
                PartTimeEmployee pte = (PartTimeEmployee)emp;
                textBoxNombre.Text = pte.Name;
                dateTimePicker.Value = pte.StartDate;
                textBoxTipo.Text = pte.HourlyRate.ToString();
            }
        }

        private void ButtonConfirmar_Click(object sender, EventArgs e)
        {
            if (textBoxNombre != null && textBoxTipo != null)
            {
                Controller c = new Controller();
                int id = ListadoPrincipal.empSeleccionado;
                Employee old = c.GetEmployee(id);
                if (old.GetType().Name == "FullTimeEmployee")
                {
                    Employee emp = new FullTimeEmployee()
                    {
                        Id = old.Id,
                        Name = textBoxNombre.Text,
                        StartDate = DateTime.Parse(dateTimePicker.Value.ToString("dd/MM/yyyy")),
                        Salary = Int32.Parse(textBoxTipo.Text)
                    };
                    c.UpdateEmployee(emp);
                    this.Close();
                }
                else
                {
                    Employee emp = new PartTimeEmployee()
                    {
                        Id = old.Id,
                        Name = textBoxNombre.Text,
                        StartDate = DateTime.Parse(dateTimePicker.Value.ToString("dd/MM/yyyy")),
                        HourlyRate = Int32.Parse(textBoxTipo.Text)
                    };
                    c.UpdateEmployee(emp);
                    this.Close();
                }
                Program.lPrincipal.refreshList();
                Program.lPrincipal.Show();
            }
        }

        private void ButtonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
            Program.lPrincipal.refreshList();
            Program.lPrincipal.Show();
        }
    }
}

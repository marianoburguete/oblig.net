﻿using Shared.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentationLayerWinform
{
    static class Program
    {
        public static ListadoPrincipal lPrincipal;
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            lPrincipal = new ListadoPrincipal();
            Application.Run(lPrincipal);
        }
    }
}
